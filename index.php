<?php require 'header.php'; ?>
<?php

    $json_as = file_get_contents('https://script.google.com/macros/s/AKfycbzp_Y_AysaJZUsXx8A0CXsya0stM-p2zuFAWNBbEPDFD1dVdlIBHoDygQTjAUEXlbx3/exec');
    $as_decoded_json =  json_decode($json_as, true);
 ?>

 <div class="container-fluid">
   <div class="row">
     <!-- <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
       <div class="position-sticky pt-3">
         <ul class="nav flex-column">
           <li class="nav-item">
             <a class="nav-link active" aria-current="page" href="#">
               <span data-feather="home"></span>
               لوحة القيادة
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="file"></span>
               الطلبات
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="shopping-cart"></span>
               المنتجات
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="users"></span>
               الزبائن
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="bar-chart-2"></span>
               التقارير
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="layers"></span>
               التكاملات
             </a>
           </li>
         </ul>

         <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
           <span>التقارير المحفوظة</span>
           <a class="link-secondary" href="#" aria-label="إضافة تقرير جديد">
             <span data-feather="plus-circle"></span>
           </a>
         </h6>
         <ul class="nav flex-column mb-2">
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="file-text"></span>
               الشهر الحالي
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="file-text"></span>
               الربع الأخير
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="file-text"></span>
               التفاعل الإجتماعي
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="#">
               <span data-feather="file-text"></span>
               مبيعات نهاية العام
             </a>
           </li>
         </ul>
       </div>
     </nav> -->

     <main class=" ">
       <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
         <h1 class="h2">AppSheet integration test</h1>
         <!-- <div class="btn-toolbar mb-2 mb-md-0">
           <div class="btn-group me-2">
             <button type="button" class="btn btn-sm btn-outline-secondary">btn 1</button>
             <button type="button" class="btn btn-sm btn-outline-secondary">btn 2</button>
           </div>
           <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
             <span data-feather="calendar"></span>
             btn 3
           </button>
         </div> -->
       </div>

       <!-- <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas> -->

       <h2>Viajes</h2>
       <div class="table-responsive">
         <table class="table table-striped table-sm">
           <thead>
             <tr>
                 <?php foreach ($as_decoded_json['GoogleSheetData'][0] as $key): ?>
                     <th scope="col"><?php echo $key; ?></th>
                 <?php endforeach; ?>
             </tr>
           </thead>
           <tbody>
               <?php
               $i = 0;
               foreach ($as_decoded_json['GoogleSheetData'] as $viaje): ?>
                   <?php if (!$i == 0): ?>
                       <tr>
                         <?php foreach ($viaje as $key): ?>
                             <td><?php echo $key; ?></td>
                         <?php endforeach; ?>
                       </tr>
                   <?php endif; ?>
               <?php
               $i++;
           endforeach; ?>

           </tbody>
         </table>
       </div>
     </main>
   </div>
 </div>

<?php require 'footer.php'; ?>
